import requests, json
import datetime
import csv

"""
Fetching ecosia report and create csv of it
"""

url = "https://s3.amazonaws.com/blog-en.ecosia.org/financial-reports/data.json"


result = requests.get(url)
text = result.text
data = json.loads(text)
del text
del result

# sort datetimes
dates = []
for key in data:
    dates.append(datetime.datetime.strptime(key, "%Y-%m"))
dates.sort()

data_list = []

for date in dates:
    month = date.month
    year = date.year

    key = f"{year}-{month}"
    d = {}
    d["date"] = date
    for k in data[key]:
        d[k] = data[key][k]
    data_list.append(d)

del dates
del data

with open("EcosiaReport.csv", "w", newline='') as f:
    writer = csv.writer(f)
    writer.writerow(["Date", 'Total Income', 'Number of trees financed', 'Expanses Green Investments', 'Operational Costs', 'Tree Expanses', 'Tax Expanses', 'Marketing Expanses'])
    for d in data_list:
        totalIncome = float(d["totalIncome"])
        numberOfTreesFinanced = int(d["numberOfTreesFinanced"])
        expanses = d["expanses"]
        expanses_greenInvestments = expanses["greenInvestments"]["amount"]
        expanses_operationalCosts = expanses["operationalCosts"]["amount"]
        expanses_trees = expanses["trees"]["amount"]
        expanses_taxes = expanses["taxes"]["amount"]
        expanses_marketing = expanses["marketing"]["amount"]

        writer.writerow([d["date"], totalIncome, numberOfTreesFinanced, expanses_greenInvestments, expanses_operationalCosts, expanses_trees, expanses_taxes, expanses_marketing])